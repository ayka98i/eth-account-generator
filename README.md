# account-generator

## Usage
```shell
//Change if a permission error occurs.
$ chmod +x geth-darwin-amd64
$ chmod +x geth-linux-amd64

// Rename the geth file depending on the OS you are using.
// For macOS
$ mv geth-darwin-amd64 geth

// For Linux
$ mv geth-linux-amd64 geth

// Run the script.
$ ./runGenerate.sh

// Input how many addresses to generate.

// If successful, the private key will be output under the keystore.
// And the information required to use the account is output to accounts.csv.
```
